﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Process = System.Diagnostics.Process;

namespace AndroidApplication5
{
    [Activity(Label = "AndroidApplication5", MainLauncher = true, Icon = "@drawable/icon")]
    public class Activity1 : Activity
    {
        int count = 1;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.MyButton);

            button.Click += delegate { button.Text = string.Format("{0} clicks!", count++); };
            TaskScheduler.UnobservedTaskException += (sender, args) =>
            {
                Console.WriteLine("Uncaught: {0}", args.Exception);
                Process.GetCurrentProcess().Kill();
            };
            Task.Run(() => Worker());
            Task.Run(() => GCPoker());
        }

        static async Task GCPoker()
        {
            for (; ; )
            {
                GC.Collect();
                await Task.Yield();
            }
        }

        static async Task Worker()
        {
            for (;;)
            {
                await FailedWebRequest();
            }
        }

        static async Task FailedWebRequest()
        {
            var client = new HttpClient();
            try
            {

                var result = await client.GetStringAsync("http://127.0.0.1/doesNOTexist");
                Console.WriteLine("Got valid result?: {0}", result);
            }
            catch (Exception e)
            {
                Console.WriteLine("Caught Exception: {0}", e.Message);
            }
        }
    }
}

